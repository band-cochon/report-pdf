#!/usr/bin/env python3

import io
import logging
import os
import sys
import time
from http import HTTPStatus

import tornado.autoreload
import tornado.ioloop
import tornado.log
import tornado.web
import weasyprint

default_format = (
    "[%(levelname)-7s] - %(name)20s - " "%(asctime)s - %(module)-20s: %(message)s"
)

logging.basicConfig(level=logging.INFO, stream=sys.stdout, format=default_format)

logger = logging.getLogger("bandcochon-report-pdf")


class GeneratePDFHandlerMixin(object):
    @classmethod
    def is_the_body_empty(cls, body):
        if not body:
            logger.error("No content given")
            return True
        return False

    @classmethod
    def generate_the_pdf_from_body(cls, body) -> bytes:
        with io.BytesIO() as content:
            weasyprint.HTML(string=body).write_pdf(content)
            return content.getvalue()


class GeneratePDFHandler(tornado.web.RequestHandler, GeneratePDFHandlerMixin):
    """
    Generate a PDF from the HTML input
    """

    def post(self):
        logger.info("POST - Request a new PDF")
        if not self.request.body:
            self.write(
                {
                    "title": "Need content",
                    "detail": "The body is empty and it should'nt",
                    "status": HTTPStatus.BAD_REQUEST,
                }
            )
            self.set_status(HTTPStatus.BAD_REQUEST)
            return

        start = time.time()
        input_html = self.request.files["content"][0]["body"]
        content = self.generate_the_pdf_from_body(input_html)

        self.set_header("Content-Type", "application/pdf")
        self.write(content)

        duration = (time.time() - start) / 1000
        logger.info("Done in %.2f sec." % duration)


def main():
    """
    Main routine
    (avoid scope pollution)
    """

    logger.info("Start web service")

    app = tornado.web.Application(
        [
            (r"/", GeneratePDFHandler),
        ]
    )

    app.listen(
        address=os.environ.get("ADDRESS", "0.0.0.0"),
        port=os.environ.get("PORT", "8888"),
    )

    if os.environ.get("DEBUG") == "1":
        tornado.autoreload.start()

    tornado.ioloop.IOLoop.current().start()


if __name__ == "__main__":
    main()
