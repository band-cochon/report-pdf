#!/usr/bin/env bash

# Launch server
python main.py &
PID=$!
sleep 2 # Wait for server started

# Start functional tests
behave
BEHAVE_STATUS=$?

# Stop every thing
kill -9 $PID
exit $BEHAVE_STATUS
