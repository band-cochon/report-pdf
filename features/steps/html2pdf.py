import io
from pathlib import Path

import PyPDF2
import requests
from behave import given, use_step_matcher, when, then

HERE = Path(__file__).resolve().parent

use_step_matcher("re")

sent_result: requests.Response


@given("A micro-service up and running")
def a_running_microservice(context):
    """
    :type context: behave.runner.Context
    """
    result = requests.get("http://localhost:8888/")
    assert result.status_code == 405


@when('I send the content of the page "resources/example.html"')
def send_content(context):
    """
    :type context: behave.runner.Context
    """
    global sent_result

    content = (HERE / "resources" / "example.html").open("rt").read()
    files = {"content": content}
    sent_result = requests.post("http://localhost:8888/", files=files)


@then("I receive a valid PDF file")
def check_pdf_file_content(context):
    """
    :type context: behave.runner.Context
    """
    with io.BytesIO(bytes(sent_result.content)) as stream:
        PyPDF2.PdfReader(stream)
